<?php


namespace App;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\CacheItem;

class App
{

    private $userIP;
    private $userAgent;
    private $language;
    private $referer;
    private $cache;
    private $width;
    private $height;

    public function __construct()
    {
        $this->userIP    = $_SERVER['REMOTE_ADDR'];
        $this->userAgent = $_SERVER['HTTP_USER_AGENT'];
        $this->referer   = empty($_SERVER['HTTP_REFERER']) ? 'test' : $_SERVER['HTTP_REFERER'];
        $this->cache     = new FilesystemAdapter('', 300);

        if (!empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) && $list = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            if (preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $list, $list)) {
                $this->language = array_combine($list[1], $list[2]);
                foreach ($this->language as $n => $v) {
                    $this->language[$n] = $v ? $v : 1;
                }
                arsort($this->language, SORT_NUMERIC);
            }
        } else {
            $this->language = array();
        }
    }

    public function run()
    {
        echo "<html>\n";
        echo "<head>\n";
        echo "<script src='http://code.jquery.com/jquery-1.9.0rc1.js'>\n";
        echo "</script>\n";
        echo "<meta http-equiv=\"refresh\" content=\"1;URL=https://apple.com/ru\"/>";
        echo "</head>\n";
        echo "<body>";
        echo "<script>";
        echo "    $( document ).ready(function() {
               witdh = screen.width;
               height = screen.height;
               referer = '$this->referer';
               $.ajax({
                   url: '/ajax.php',
                   data: {width:witdh,height:height,referer:referer},
                   type: 'post',
               });            
            });
            ";
        echo "</script>";
        echo "</body>\n";
        echo "</html>\n";

    }

    public function ajax()
    {
        $this->referer = empty($_POST['referer']) ? '' : $_POST['referer'];
        $this->width   = empty($_POST['width']) ? '' : $_POST['width'];
        $this->height  = empty($_POST['height']) ? '' : $_POST['height'];
        //$res  = preg_match('/X\s(\w*)\)/', $this->userAgent, $ver);
        $res  = preg_match('/OS\s(\w*)\slike/', $this->userAgent, $ver);
        $ver1 = empty($ver[1]) ? null : $ver[1];
        if ($ver1) {
//            $hash = md5($this->userIP.'+'.$ver1.'+'.$this->width.'+'.$this->height.'+'.$this->language);
            $hash = md5($this->userIP . '+' . $ver1 . '+' . $this->width . '+' . $this->height);
            $hash = $this->cache->getItem($hash);
            /**
             * @var CacheItem $hash
             */
            $hash->set($this->referer);
            $isSaved = $this->cache->save($hash);
            print_r($isSaved);
        }
    }

    public function income()
    {
        $this->width   = empty($_POST['width']) ? '' : $_POST['width'];
        $this->height  = empty($_POST['height']) ? '' : $_POST['height'];
        $ver  = empty($_POST['ver']) ? '' : $_POST['ver'];
        $hash = md5($this->userIP . '+' . $ver . '+' . $this->width . '+' . $this->height);
        $hash = $this->cache->getItem($hash);
        /**
         * @var CacheItem $hash
         */
        $referer = $hash->get();
        if ($referer) {
            print_r($referer);
            $this->cache->clear();
        } else {
            print_r('error hash');
        }
    }
}